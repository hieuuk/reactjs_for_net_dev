﻿class ProductItem extends React.Component {
    state = {
        Hot: this.props.product.Hot
    }

    toggleHighlight = () => {
        this.setState(state => ({
            Hot: !this.state.Hot
        }));
    }

    render() {
        let product = this.props.product;
        return (
            <div className="col-md-3 product-item">
                {product.Name} <br />
                <strong>{product.Price}</strong><br/>
                <button
                    onClick={this.toggleHighlight}
                    className={"btn btn-sm " + (this.state.Hot ? "btn-warning" : "btn-dark")}>Hot</button>
                <br/>
                <button
                    onClick={this.props.onRemove}
                    className="btn btn-sm btn-danger">
                    Remove
                </button>
            </div>
        );
    }
}

class ProductList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Products: [
                {
                    Id: 1,
                    Name: "Expert",
                    Price: 100,
                    Hot: true,
                    Visible: true
                },
                {
                    Id: 2,
                    Name: "Keeper",
                    Price: 50,
                    Hot: true,
                    Visible: true
                },
                {
                    Id: 3,
                    Name: "Master",
                    Price: 80,
                    Hot: true,
                    Visible: true
                }
            ]
        }
    }

    removeProduct = function (product) {
        let products = this.state.Products;
        console.log(products);
        products = products.filter(function (item) {
            return item.Id !== product.Id;
        });
        console.log(products);
        this.setState(state => ({
            Products: products
        }))
    }

    render() {
        return (
            <div id="product-list" className="row">
                {
                    this.state.Products.map(function (item, i) {
                        return <ProductItem key={item.Id} product={item} onRemove={this.removeProduct.bind(this, item)} />
                    }, this)
                }
            </div>
        );
    }
}

ReactDOM.render(<ProductList />, document.getElementById('root'))